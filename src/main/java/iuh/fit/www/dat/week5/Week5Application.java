package iuh.fit.www.dat.week5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Week5Application {

	public static void main(String[] args) {
		SpringApplication.run(Week5Application.class, args);
	}

}
