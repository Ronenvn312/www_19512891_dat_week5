package iuh.fit.www.dat.week5.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "job_skill")
public class JobSkill {

    @EmbeddedId
    JobSkillKey jobSkillKey;

    @ManyToOne
    @MapsId("jobId")
    @JoinColumn(name = "job_id")
    Job job;

    @ManyToOne
    @MapsId("skillId")
    @JoinColumn(name = "skill_id")
    Skill skill;

    @Column(name = "more_infos")
    private String moreInfos;

    @Column(name = "skill_level")
    private int skillLevel;
}
