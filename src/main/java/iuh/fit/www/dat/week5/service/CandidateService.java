package iuh.fit.www.dat.week5.service;

import iuh.fit.www.dat.week5.model.Candidate;

import java.util.List;

public interface CandidateService {

    List<Candidate> getAll();
}
