package iuh.fit.www.dat.week5.controller;

import iuh.fit.www.dat.week5.model.Candidate;
import iuh.fit.www.dat.week5.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("candidate")
public class CandidateController {

    @Autowired
    private CandidateService candidateService;

    @GetMapping("/findAll")
    public List<Candidate> findAllCandidate() {
        return candidateService.getAll();
    }
}
