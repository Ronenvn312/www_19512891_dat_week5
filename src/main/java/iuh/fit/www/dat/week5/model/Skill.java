package iuh.fit.www.dat.week5.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "skill")
public class Skill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "skill_id")
    private long skillId;

    @Column(name = "skill_description")
    private String skillDescription;

    @Column(name = "skill_name")
    private String skillName;

    @Column(name = "type")
    private String type;

    @OneToMany(mappedBy = "skill", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    Set<JobSkill> jobSkills;

    @OneToMany(mappedBy = "skill", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    Set<CandidateSkill> candidateSkills;
}
