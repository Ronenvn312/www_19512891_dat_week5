package iuh.fit.www.dat.week5.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;

import java.io.Serializable;

@Embeddable
public class JobSkillKey implements Serializable {

    @Column(name = "job_id")
    long jobId;

    @Column(name = "skill_id")
    long skillId;

}
