package iuh.fit.www.dat.week5.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comp_id")
    private long id;

    @Column(name = "about")
    private String about;

    @Column(name = "email")
    private String email;

    @Column(name = "comp_name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "web_url")
    private String webUrl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;

    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private List<Job> jobList = new ArrayList<>();


}
