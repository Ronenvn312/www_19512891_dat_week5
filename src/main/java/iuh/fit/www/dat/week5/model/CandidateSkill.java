package iuh.fit.www.dat.week5.model;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "candidate_skill")
public class CandidateSkill {

    @EmbeddedId
    CandidateSkillKey candidateSkillKey;

    @ManyToOne
    @MapsId("canId")
    @JoinColumn(name = "can_id")
    Candidate candidate;

    @ManyToOne
    @MapsId("skillId")
    @JoinColumn(name = "skill_id")
    Skill skill;

    @Column(name = "more_infos")
    private String moreInfos;

    @Column(name = "skill_level")
    private int skillLevel;
}
