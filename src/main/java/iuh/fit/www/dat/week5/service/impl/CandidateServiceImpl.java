package iuh.fit.www.dat.week5.service.impl;

import iuh.fit.www.dat.week5.model.Candidate;
import iuh.fit.www.dat.week5.repository.CandidateRepository;
import iuh.fit.www.dat.week5.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;

    @Override
    public List<Candidate> getAll() {
        return candidateRepository.findAll();
    }
}
