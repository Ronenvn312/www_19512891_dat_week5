package iuh.fit.www.dat.week5.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "address")
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private long id;

    @Column(name = "street")
    private String street;

    @Column(name = "city")
    private String city;

    @Column(name = "country")
    private int country;

    @Column(name = "number")
    private String number;

    @Column(name = "zipcode")
    private String zipcode;

    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private List<Candidate> candidateList = new ArrayList<>();

    @OneToMany(mappedBy = "address", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private List<Company> companyList = new ArrayList<>();

}
